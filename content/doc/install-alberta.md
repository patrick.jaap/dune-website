+++
title = "Installing ALBERTA"
+++

### Installing ALBERTA for DUNE

[ALBERTA](http://www.alberta-fem.de) is an Adaptive multiLevel finite element
toolbox using Bisectioning refinement and Error control by Residual Techniques
for scientific Applications. DUNE offers an abstraction to the ALBERTA grid
manager through the AlbertaGrid class.

You should use ALBERTA's
[code repository](https://gitlab.mathematik.uni-stuttgart.de/ians-nmh/alberta/alberta3)
to download their
[latest release](https://gitlab.mathematik.uni-stuttgart.de/ians-nmh/alberta/alberta3/-/releases).


### Prebuild packages

+ Debian: The package is called libalberta-dev.

#### Compilation and Installation

ALBERTA has a standard Autotools build system.

1. Unpack the release tarball and enter the main directory

2. Build the ALBERTA make system
    ```
     ./configure --prefix=my_favourite_alberta_installation_path --disable-fem-toolbox
    ```

    * --prefix determines the absolute path of the directory we install to.
    * --disable-fem-toolbox disables the FEM toolbox which is not used in DUNE.

    Using the variables CC and CXX, you can tell configure what C and C++ compiler to use. These have to be the same compilers and linkers you will use to compile DUNE.

    More information and additional options can be found in the configure script's help, displayed by
    ```
     ./configure --help
    ```

3. Compile and install ALBERTA with
    ```
    make install
    ```

3. Add
    ```
    CMAKE_FLAGS+=" -DCMAKE_PREFIX_PATH=my_favourite_alberta_installation_path "
    ```
    as a CMake option for dune-grid.
