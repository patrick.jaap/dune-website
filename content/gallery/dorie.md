+++
title = "Soil Water Flow and Passive Solute Transport"
content = "carousel"
image = ["/img/dorie-adaptive.png", "/img/dorie-solute.png"]
+++

[DORiE](/modules/dorie) simulation results for infiltration into a
small-scale heterogeneous, hill-shaped domain (left), and for evaporation of
contaminated water from a domain filled with a coarse and a fine-grained
material (right) [(Riedel et al. 2020)](https://doi.org/10.21105/joss.02313).
