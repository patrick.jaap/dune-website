+++
date = "2017-07-18"
title = "Dune 2.5.1 Released"
+++

The <span style="font-variant: small-caps">Dune</span> 2.5.1
maintenance release is out.  You can [download the tarballs], checkout
the `v2.5.1` tag via Git, or get prebuilt packages from Debian
unstable.

Included in the release are the [core modules][] (dune-common,
dune-geometry, dune-grid, dune-grid-howto, dune-istl, dune-localfunctions)
and several modules from the [staging area][] (dune-functions, dune-typetree,
dune-uggrid).

Please refer to the [release notes] for a more detailed listing of
what has changed in the new release.

  [download the tarballs]: /releases/2.5.1
  [core modules]: https://gitlab.dune-project.org/core/
  [staging area]: https://gitlab.dune-project.org/staging/
  [release notes]: /releases/2.5.1#dune-2-5-1-release-notes
