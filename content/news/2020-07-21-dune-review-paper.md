+++
date = "2020-07-21"
title = "The Dune framework: Basic concepts and recent developments"
tags = [ "publications", "core", "review" ]
+++

This recently published open-access paper
"__[The Dune framework: Basic concepts and recent developments][dune review paper]__"
presents the basic concepts and the module structure of the Distributed and
Unified Numerics Environment (DUNE) and reflects on recent
developments and general changes that happened since the release
of the first Dune version in 2007 and the main papers
describing that state [(Bastian et al. 2008)][dune core paper].
This discussion is accompanied with a description of
various advanced features, such as coupling of domains and cut cells, grid
modifications such as adaptation and moving domains,
high order discretizations and node level performance,
non-smooth multigrid methods, and multiscale methods. A brief
discussion on current and future development directions
of the framework concludes the paper.

[dune review paper]: https://doi.org/10.1016/j.camwa.2020.06.007
[dune core paper]: https://opus4.kobv.de/opus4-matheon/frontdoor/index/index/docId/416
