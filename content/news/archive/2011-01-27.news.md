+++
date = "2011-01-27"
title = "DUNE Spring School, April 11-15 2011"
+++

University of Freiburg, Germany

This one week course will give an introduction to the DUNE core modules including the DUNE grid interface library, and the [DUNE-FEM](http://dune.mathematik.uni-freiburg.de/) module.

The course will focus on the numerical treatment of evolution equations of the form

∂<sub>t</sub>U + ∇ · ( F(U) - D(U) ∇U ) + S(U) = 0

using continuous and discontinuous Galerkin methods. These schemes will be implemented with the DUNE-FEM module. After an introduction to the DUNE grid interface and DUNE-FEM, the final third part of the course will highlight on the discretization of PDEs on surfaces.

Further information and a registration from can be found [here](http://dune.mathematik.uni-freiburg.de/schools/).
