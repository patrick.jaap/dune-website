+++
date = "2014-03-28"
title = "Dune-Ash at the Deutsche Technik Museum Berlin"
+++

We are happy to announce that the special exhibition '[Mathematics of Planet Earth](http://www.sdtb.de/Mathematics-of-Planet-Earth.2312.0.html)' at the Deutsche Technik Museum Berlin opened yesterday. Among other things, it exhibits the Dune based interactive volcano ash simulation [Dune-Ash](http://www.dune.uni-freiburg.de/ash/) until June 30th.

Dune-Ash is also on permanent display in the [MiMa Oberwolfach](http://www.mima.museum). The aim of the developers of Dune-Ash is to illustrate what mathematics and software simulation tools like Dune are used for in everyday life.

Further exhibitions all over the world are planned.
