+++
date = "2016-05-01"
title = "Getting started with Dune"
+++

There is a new text on [how to get started with Dune](https://tu-dresden.de/mn/math/numerik/sander/ressourcen/dateien/sander-getting-started-with-dune-2-7.pdf). We hope that it will be useful to many people.
