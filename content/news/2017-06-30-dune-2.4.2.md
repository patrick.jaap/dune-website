+++
date = "2017-06-30"
title = "DUNE 2.4.2 released"
+++

<span style="font-variant: small-caps">Dune</span> 2.4.2 has been
released.  You can [download the tarballs] of checkout the `v2.4.2`
tag via Git.

Included in the release are the [core modules][] (dune-common,
dune-geometry, dune-grid, dune-istl, dune-localfunctions).

<span style="font-variant: small-caps">Dune</span> 2.4.2 is a bug
fix release to keep <span style="font-variant: small-caps">Dune</span>
2.4 working for recent systems. It is advised to move to
<span style="font-variant: small-caps">Dune</span> 2.5.

  [download the tarballs]: /releases/2.4.2
  [core modules]: https://gitlab.dune-project.org/core/
