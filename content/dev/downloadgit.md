+++
title = "Download via Git"
[menu.main]
parent = "dev"
weight = 2
+++
## Download unstable DUNE modules using Git

DUNE consists of several separate modules. Depending on the application you are writing you will need a certain set of the modules.

DUNE development happens using Git. If you want access to the current development version, you can clone the Git repositories of the DUNE modules. All modules are dependent on the base module dune-common and hence it is necessary to have dune-common in all cases.

The central place for development is our GitLab instance at http://gitlab.dune-project.org. It hosts the main repositories for the core modules, its bug trackers and is the preferred place for contributions. For visibility reasons, the repositories for the core modules are mirrored on [GitHub](https://github.com/dune-project).

If you want to contribute back to DUNE (great - we are looking forward to it!), please make sure to read about our [whitespace policy](/doc/guides/whitespace_hook) and go to [how to contribute](/dev/contributing)
### Core Modules

* [dune-common](/modules/dune-common) (basic classes)<br>
   `git clone https://gitlab.dune-project.org/core/dune-common.git` ([Browse Repository](https://gitlab.dune-project.org/core/dune-common))
* [dune-geometry](/modules/dune-geometry) (element geometries)<br>
  `git clone https://gitlab.dune-project.org/core/dune-geometry.git` ([Browse Repository](https://gitlab.dune-project.org/core/dune-geometry))
* [dune-grid](/modules/dune-grid) (abstract grid/mesh interface)<br>
  `git clone https://gitlab.dune-project.org/core/dune-grid.git` ([Browse Repository](https://gitlab.dune-project.org/core/dune-grid))
* [dune-istl](/modules/dune-istl) (Iterative Solver Template Library)<br>
  `git clone https://gitlab.dune-project.org/core/dune-istl.git` ([Browse Repository](https://gitlab.dune-project.org/core/dune-istl))
* [dune-localfunctions](/modules/dune-localfunctions) (Interface for the local basis and layout of the degrees of freedom))<br>
  `git clone https://gitlab.dune-project.org/core/dune-localfunctions.git` ([Browse Repository](https://gitlab.dune-project.org/core/dune-localfunctions))
