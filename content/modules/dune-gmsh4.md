+++
module = "dune-gmsh4"
group = ["extension"]

# List of modules that this module requires
requires = ["dune-grid"]
suggests = ["dune-alugrid", "dune-foamgrid", "dune-vtk", "dune-curvedgrid"]
maintainers = "[Simon Praetorius](mailto:simon.praetorius@tu-dresden.de), [Florian Stenger](florian.stenger@tu-dresden.de)"

# Main Git repository, uncomment if present
git = "https://gitlab.mn.tu-dresden.de/iwr/dune-gmsh4"

short = "File reader for the Gmsh-4 file format"

+++
# GMsh4-Reader
This module provides file readers and grid creators for the Gmsh .msh file format in version 4.

## Example of usage
Either construct a grid directly
```c++
#include <dune/gmsh4/gmsh4reader.hh>
#include <dune/gmsh4/gridcreators/lagrangegridcreator.hh>
#include <dune/grid/uggrid.hh>

int main()
{
  using namespace Dune;
  using Grid = UGGrid<2>;
  std::unique_ptr grid = Gmsh4Reader<Grid>::createGridFromFile("filename.msh");
}
```
or explicitly use a grid-creator
```c++
#include <dune/foamgrid/foamgrid.hh>
#include <dune/gmsh4/gmsh4reader.hh>
#include <dune/gmsh4/gridcreators/lagrangegridcreator.hh>

int main()
{
  using namespace Dune;
  using Grid = FoamGrid<2,3>;
  GridFactory<Grid> factory;

  // The creator is responsible for filling a GridFactory from the data
  // found in the file. Additionally, it might provide a grid-function representing
  // a geometry parametrization
  Gmsh4::LagrangeGridCreator creator{factory};
  Gmsh4Reader reader{creator};

  reader.read("filename.msh");
  auto grid = factory.createGrid();
}
```
Note, the grid-creator itself is a grid-function.
